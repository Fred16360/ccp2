Pour le CCP2, j'ai réalisé la partie "Blog".

Pour saisir une publication, il faut s'enregistrer en tant que rédacteur ou responsable éditorial.

Les visiteurs du site peuvent laisser un ou plusieurs commentaires à propos d'une publication sans s'enregistrer.
Par contre, tous les commentaires sont sujet à validation par le resposanble éditorial et seulement lui.

Lien pour se connecter à l'administration du site :
https://ccp2.marzat-informatique.fr/administration

Connexion en tant que rédacteur :
email : redacteur@test.com
mdp : demodemo

Connexion en tant que responsable éditorial :
email : responsable@test.com
mdp : demodemo
