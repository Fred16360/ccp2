from django.apps import AppConfig


class WebagencyConfig(AppConfig):
    name = 'webagency'
