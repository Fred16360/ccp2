from django.shortcuts import render, redirect, get_object_or_404
from blog.forms import CommentaireForm, PublicationForm, PublicationFormUpdate
from blog.models import Publication, Commentaire
from django.contrib import messages


# Create your views here.

def blog_index(request):
    posts = Publication.objects.all().order_by('-created_on')
    return render(request, "blog_index.html", { "posts": posts })


def post_detail(request, pk):
    try:
        post = Publication.objects.get(pk=pk)
        comments = Commentaire.objects.filter(post=post)
    except Publication.DoesNotExist:
        return redirect('blog_index')
    form = CommentaireForm()
    if request.method == 'POST':
        form = CommentaireForm(request.POST or None)
        if form.is_valid():
            comment = Commentaire (
            auteur=form.cleaned_data["auteur"],
            texte=form.cleaned_data["texte"],
            post=post,
            )
            comment.save()
            messages.add_message(request, messages.SUCCESS, 'Merci pour votre commentaire')
        form = CommentaireForm()
      
    context = {"post": post,"comments": comments, "form": form}
    return render(request, 'post_detail.html', context)


def validation_commentaire(request):
    nonvalider = Commentaire.objects.all().exclude(validation=True)
    return render(request, 'validation_commentaire.html', {'nonvalider': nonvalider})


def commentaire_detail(request, pk):
    try:
        comment = Commentaire.objects.get(pk=pk)
    except Commentaire.DoesNotExist:
        return redirect('validation_commentaire')
    if request.method == 'POST':
        comment.validation = True
        comment.save()
        return redirect('validation_commentaire')
    return render(request, 'commentaire_detail.html', {'comment': comment} )