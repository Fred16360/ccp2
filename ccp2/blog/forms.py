from django import forms
from tinymce.widgets import TinyMCE

from .models import Publication, Categorie, Commentaire


class CommentaireForm(forms.ModelForm):
    class Meta:
        model = Commentaire
        fields = '__all__'
    auteur = forms.CharField(
    max_length=60,
    widget=forms.TextInput(attrs={
        "class": "form-control",
        "placeholder": "Votre nom"
        })
    )
    texte = forms.CharField(
    widget=TinyMCE(attrs={
        "class": "form-control"
        })
    )
    post = forms.IntegerField(required=False)
    validation = forms.BooleanField(required=False)


class PublicationForm(forms.ModelForm):
    class Meta:
        model = Publication
        fields = ['titre','texte','categorie','image']
    publish = forms.IntegerField(required=False)
    titre = forms.CharField(
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )
    image = forms.ImageField(
        widget=forms.FileInput(attrs={
        })
    )


class PublicationFormUpdate(forms.ModelForm):
    class Meta:
        model = Publication
        fields = '__all__'
    publish = forms.IntegerField(required=False)
    titre = forms.CharField(
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )
    texte = forms.CharField(
        widget=forms.Textarea(attrs={
            "class": "form-control",
        })
    )
    image = forms.ImageField(
        required=False,
        widget=forms.FileInput(attrs={
        })
    )


class CategorieForm(forms.ModelForm):
    class Meta:
        model = Categorie
        fields = '__all__'
    nom_categorie = forms.CharField(
        widget=forms.TextInput(attrs={
            "class": "form-control"
        })
    )


class PublicationFormDelete(forms.ModelForm):
    class Meta:
        model = Publication
        fields = '__all__'
    titre = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
        "class": "form-control font-weight-bold",
        "disabled": "disabled",
        })
    )
    texte = forms.CharField(
        required=False,
        widget=forms.Textarea(attrs={
            "class": "form-control",
            "disabled": "disabled",
        })
    )
    image = forms.ImageField(
        required=False,
        widget=forms.FileInput(attrs={
        })
    )    