from django.db import models
from tinymce.models import HTMLField
from tinymce import models as tinymce_models

from accounts.models import Account

# Create your models here.
class Categorie(models.Model):
    nom_categorie = models.CharField(max_length=25, verbose_name='Nom catégorie')

    def __str__(self):
        return self.nom_categorie


class Publication(models.Model):
    publish = models.ForeignKey(Account, on_delete=models.CASCADE, null=True)
    titre = models.CharField(max_length=255)
    texte = tinymce_models.HTMLField()
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    categorie = models.ForeignKey('Categorie', on_delete=models.CASCADE, null=True)
    image = models.ImageField()

    def __str__(self):
        return self.titre


class Commentaire(models.Model):
    auteur = models.CharField(max_length=60)
    texte = tinymce_models.HTMLField()
    created_on = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey('Publication', on_delete=models.CASCADE)
    validation = models.BooleanField(default=False)


    