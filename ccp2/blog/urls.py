from django.urls import path
from django.conf.urls.static import static

from blog import views


urlpatterns = [
    path('liste/', views.blog_index, name='blog_index'),
    path("post/<int:pk>/", views.post_detail, name="post_detail"),
    path('post/valid/', views.validation_commentaire, name='validation_commentaire'),
    path('post/commentaire/detail/<int:pk>/', views.commentaire_detail, name='commentaire_detail'),
]