from django.contrib import admin
from .models import Categorie, Commentaire, Publication


# Register your models here.
class CategorieAdmin(admin.ModelAdmin):
    list_display = ['nom_categorie']

admin.site.register(Categorie, CategorieAdmin)


class PublicationAdmin(admin.ModelAdmin):
    list_display = ['titre', 'created_on']

admin.site.register(Publication, PublicationAdmin)


class CommentaireAdmin(admin.ModelAdmin):
    model = Commentaire
    list_display = ['auteur', 'created_on', 'post', 'commentaire', 'validation']
    list_display_links = ('auteur','commentaire')
    
    def commentaire(self, obj):
        return obj.texte[:20]

admin.site.register(Commentaire, CommentaireAdmin)

