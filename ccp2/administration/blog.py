from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django import forms

from blog.forms import CommentaireForm, PublicationForm, PublicationFormUpdate, PublicationFormDelete, CategorieForm
from blog.models import Publication, Commentaire, Categorie


@login_required(login_url='../../loginadmin/')
def blog_index_admin(request):
    posts = Publication.objects.all().order_by('-created_on')
    return render(request, "blog/blog_index_admin.html", { "posts": posts })


@login_required(login_url='../../loginadmin/')
def post_create(request):
    form = PublicationForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.instance.publish = request.user
        form.save()
        return redirect('blog_index_admin')
    return render(request, "blog/post_create_update.html", {"form": form})


@login_required(login_url='../../loginadmin/')
def post_update(request, pk):
    try:
        post = Publication.objects.get(pk=pk)
    except Publication.DoesNotExist:
        return redirect('blog_index_admin')
    form = PublicationFormUpdate(request.POST or None, request.FILES or None, instance=post)
    if form.is_valid():
        form.instance.publish = request.user
        form.save()
        return redirect('blog_index_admin')
    return render(request, "blog/post_create_update.html", {"form": form})


@login_required(login_url='../../loginadmin/')
def post_delete(request, pk):
    try:
        post = Publication.objects.get(pk=pk)
    except Publication.DoesNotExist:
        return redirect('blog_index_admin')
    form = PublicationFormDelete(request.POST or None, request.FILES or None, instance=post)
    if request.method == 'POST':
        post.delete()
        return redirect('blog_index_admin')
    return render(request, "blog/post_delete.html", {"form": form})    


@login_required(login_url='../../loginadmin/')
def categorie_create(request):
    try:
        form = CategorieForm(request.POST or None)
    except Categorie.DoesNotExist:
        return redirect('categorie_liste')
    if form.is_valid():
        form.save()
        return redirect('categorie_liste')
    return render(request, 'blog/categorie_create.html', {'form': form})


@login_required(login_url='../../../loginadmin/')
def categorie_liste(request):
    try: 
        categorie = Categorie.objects.all()
    except Categorie.DoesNotExist:
        return redirect('blog_index_admin')
    return render(request, 'blog/categorie_liste.html', {'categorie': categorie})


@login_required(login_url='../../../loginadmin/')
def categorie_update(request, pk):
    try:
        categorie = Categorie.objects.get(pk=pk)
    except Categorie.DoesNotExist:
        return redirect('categorie_liste')
    form = CategorieForm(request.POST or None, instance=categorie)
    if form.is_valid():
        form.save()
        return redirect('categorie_liste')
    return render(request, 'blog/categorie_update.html', {'form': form, 'pk': pk})    


@login_required(login_url='../../../loginadmin/')
def categorie_delete(request, pk):
    try:
        categorie = Categorie.objects.get(pk=pk)
    except Categorie.DoesNotExist:
        return redirect('categorie_liste')
    categorie.delete()
    return redirect('categorie_liste')
