from django.urls import path
from django.contrib.auth.views import LoginView

from . import views
# Dans l'administration un fichier par nom d'application est créé pour ne pas avoir un fichier views très lourd
from administration import blog   # importation de blog.py


urlpatterns = [
    path('loginadmin/', LoginView.as_view(template_name='login_admin.html'), name='login_admin'),
    path('', views.index_admin, name='index_admin'),

    path('blog/liste/', blog.blog_index_admin, name='blog_index_admin'),
    path('blog/post/create/', blog.post_create, name='post_create'),
    path('blog/post/update/<int:pk>', blog.post_update, name='post_update'),
    path('blog/post/delete/<int:pk>', blog.post_delete, name='post_delete'),

    path('blog/categorie/create/', blog.categorie_create, name='categorie_create'),
    path('blog/categorie/liste/', blog.categorie_liste, name='categorie_liste'),
    path('blog/categorie/update/<int:pk>', blog.categorie_update, name='categorie_update'),
    path('blog/categorie/delete/<int:pk>', blog.categorie_delete, name='categorie_delete'),
]