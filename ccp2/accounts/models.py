from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth import get_user_model
from django.utils import timezone


class AccountManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, nom, prenom, password, **extra_fields):
        values = [email, nom, prenom]
        field_value_map = dict(zip(self.model.REQUIRED_FIELDS, values))
        for field_name, value in field_value_map.items():
            if not value:
                raise ValueError('The {} value must be set'.format(field_name))

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            nom=nom,
            prenom=prenom,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, nom, prenom, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, nom, prenom, password, **extra_fields)

    def create_superuser(self, email, nom, prenom, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, nom, prenom, password, **extra_fields)


class Account(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    nom = models.CharField(max_length=50, verbose_name="Nom")
    prenom = models.CharField(max_length=50, verbose_name="Prénom")
    fonction = models.CharField(max_length=50, verbose_name="Fonction")
    service = models.CharField(max_length=50, verbose_name="Service")
    tel_fixe = models.CharField(max_length=25, verbose_name="Téléphone fixe")
    tel_portable = models.CharField(max_length=25, verbose_name="Téléphone portable")
    TYPE_USER_CHOIX = [
        ('VIS', 'Visiteur'),
        ('RED', 'Rédacteur'),
        ('RES', 'Responsable éditorial'),
        ('PRE', 'Prestataire'),
    ]
    type_user = models.CharField(choices=TYPE_USER_CHOIX, default="VIS", max_length=3)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(null=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nom', 'prenom']

    def get_full_name(self):
        return self.nom

    def get_short_name(self):
        return self.nom.split()[0]

    def __str__(self):
        return self.nom + ' ' + self.prenom
